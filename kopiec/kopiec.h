#ifndef _KOPIEC_   
#define _KOPIEC_  

#include <iostream>
#include <string>
using namespace std;

template <typename E>
class kopiec { 

private:
	E*tab;
	int size;
	int size_max;
public:
	kopiec()
	{
		size = 1;
		tab = new E[1];
		size_max = 1;
	}

	kopiec(E *tablica,int n)
	{
		tab = tablica;
		size = n;
		size_max = n;
		for (int i = n/2 ;i > 0;i--)
			downheap(i);
	}

	~kopiec()
	{
		;
	}

	void set_tab(E*Tab)
	{
		tab = Tab;
	}

	E* get_tab()
	{
		return tab;
	}

	int get_size()
	{
		return size;
	}

	int get_size_max()
	{
		return size_max;
	}

	bool empty()
	{
		if (tab == NULL)
			return true;
		else
			return false;
	}

	void upheap(int iterator)
	{
		if (iterator <= 1 || iterator >= size)
			return;
		else
		{
			if (tab[iterator] > tab[iterator / 2])  // dzielone przez 2 bo i tak int zaokrogli w dol nawet przy prawym synu (syn>ojciec)
			{
				E tmp = tab[iterator];					//zamiana ojca z synem i wywolanie rekurencyjne
				tab[iterator] = tab[iterator / 2];
				tab[iterator / 2] = tmp;
				upheap(iterator / 2);
			}
		}
	}

	void downheap(int iterator)
	{
		if (iterator < 1 || iterator * 2 >= size)
			return;
		else
		{
			if (tab[iterator] < tab[iterator * 2]|| tab[iterator] < tab[iterator * 2 + 1])      // spr czy lewy syn jst mniejszy od ojca  (ojciec<syn)
			{
				if (tab[iterator*2] < tab[iterator * 2 + 1])      // spr czy prawy syn jst mniejszy od ojca  (ojciec<syn)
				{
					E tmp = tab[iterator];					  //zamiana ojca z synem i wywolanie rekurencyjne
					tab[iterator] = tab[iterator * 2 + 1];
					tab[iterator * 2 + 1] = tmp;
					downheap(iterator * 2 + 1);
				}
				else
				{
					E tmp = tab[iterator];					  //zamiana ojca z synem i wywolanie rekurencyjne
					tab[iterator] = tab[iterator * 2];
					tab[iterator * 2] = tmp;
					downheap(iterator * 2);
				}
			}

		}
	}

	void push(E ele)
	{
		if (size == size_max)
		{
			E*tmp = get_tab();
			size_max = 2 * get_size_max();
			set_tab(new E[get_size_max()]);
			for (int i = 0;i < get_size(); i++)
				tab[i] = tmp[i];
			get_tab()[get_size()] = ele;
			delete[] tmp;
		}
		else
			tab[size] = ele;
		size++;
		upheap(size-1);
	}

	E remove_root()
	{
		if (size <= 1)
		{
			string wyjatek = "nie ma elementow do zdjecia!";
			throw wyjatek;
		}
		/*else if(2*size<size_max)
		{
			E ele = get_tab()[1];
			tab[1] = tab[get_size() - 1];
			E*tmp = get_tab();
			set_tab(new E[get_size() - 1]);
			for (int i = 0;i < get_size() - 1; i++)
				tab[i] = tmp[i];
			delete[] tmp;
			size--;
			size_max = get_size();
			downheap(1);
			return ele;
		}*/
		else
		{
			E ele = tab[1];			 
			tab[1] = tab[size-1];
			size--;
			downheap(1);
			return ele;
		}

	}

	void print()
	{
		for (int i = 1;i< get_size();++i)
			cout <<i<<":  "<< tab[i] << endl;
	}

};




#endif 