#include "kopiec.h"
#include "time.h"
#include "lista_dwukierunkowa.h"
#include <math.h> 
#include <time.h>

template <typename E>
void kopcowanie(E *tab,int n)
{
	kopiec<E> k(tab, n);
	for (int i = n-1;i >0;i--)
		tab[i] = k.remove_root();

}

template <typename E>
void podziel(Deque<E> &l, Deque<E> &p, Deque<E> &tab)
{
	
	if (tab.size > 1) {
		if (tab.size % 2 == 0)
		{
			l.head = tab.head;
			l.tail = tab(tab.size / 2-1);
			l.size = tab.size / 2;
			wezel<E>* tmp = l.tail->get_next();
			l.tail->set_next(NULL);
			p.head = tmp;
			p.tail = tab.tail;
			p.size = tab.size / 2;
			p.head->set_back(NULL);
		}
		else
		{
			l.head = tab.head;
			l.tail = tab(tab.size / 2 );
			l.size = tab.size / 2+1 ;
			wezel<E> *tmp = l.tail->get_next();
			l.tail->set_next(NULL);
			p.head = tmp;
			p.tail = tab.tail;
			p.size = tab.size / 2;
			p.head->set_back(NULL);
		}
	}

}

template <typename E>
void polacz(Deque<E> &l, Deque<E> &p, Deque<E> &s)
{
	while (!l.empty() && !p.empty())
	{
		if (l[0] <= p[0])
			s.add_back(l.front());
		else
			s.add_back(p.front());
	}

	while (!l.empty())
		s.add_back(l.front());

	while (!p.empty())
		s.add_back(p.front());
}

template <typename E>
void swap(E *tab, int a, int b)
{
	E tmp = tab[a];
	tab[a] = tab[b];
	tab[b] = tmp;
}

template <typename typ>
int podzial_in_situ(typ *tab, int a, int b)
{
	int l = a;
	int r = b - 1;
	while (1)
	{
		while (tab[l] < tab[b]&&r>=l)
			l++;
		while (tab[r] > tab[b]&&r>=l)
			r--;
		if (r > l)
		{
			typ tmp = tab[r];
			tab[r] = tab[l];
			tab[l] = tmp;
			l++;
			r--;
		}
		else
		{
			typ tmp = tab[l];
			tab[l] = tab[b];
			tab[b] = tmp;
		
			return l;
		}
	}
}
//====================================================================================================================
template <typename E>
void szybkie(E *tab, int a, int b)
{
	if (a > b)
		return;
	//int i= (a+b)/2;
	E tmp = tab[a];
	tab[a] = tab[b];
	tab[b] = tmp;
	int l = podzial_in_situ(tab+1, a, b);
	if (l != a)
		szybkie(tab, a, l - 1);
	if (l != b)
		szybkie(tab, l + 1, b);

}

template <typename E>
void introspektywne(E *tab, int a, int b, int n)
{
	if (n < 0) {
		//cout << "KOPIEC!!!!!!!!!!!!!!!!!!!!!!!!!!!!: \n";
		kopcowanie(tab, b + 1);
		return;
	}
	if (a > b)
		return;
	//int i = (a + b) / 2;
	E tmp = tab[a];
	tab[a] = tab[b];
	tab[b] = tmp;
	int l = podzial_in_situ(tab, a, b);
	if (l != a)
		introspektywne(tab, a, l - 1,n-1);
	if (l != b)
		introspektywne(tab, l + 1, b, n - 1);
}

template <typename E>
Deque<E> scal(Deque<E> &tab)
{
	if (tab.get_size() <= 1)
		return tab;
	else
	{
		Deque <E> l, p, s;
		podziel(l, p, tab);
		polacz(scal(l), scal(p), s);
		tab = s;
		return s;
	}
}


#pragma comment(linker, "/STACK:100000000000")

int main()
{
	srand((unsigned int)time(NULL));
	try {
		int n[5] = { 10000,50000,100000,500000,1000000 };
		double proc[8] = { 0, 0.25,0.5,0.75,0.95,0.99,0.997,-1 };
		clock_t start, stop;



		for (int l = 0;l < 5;l++)
		{
		int *sort1 = new int[n[l]];
		int *sort2 = new int[n[l] + 1];
		Deque<int> sort3;

		double szyb[100];
		double intro[100];
		double scalanie[100];
		double srednia;

		for (int k = 0;k < 8;k++)
		{
		cout << endl << "sredni czas sorotwan tablicy " << n[l] << " elementow. wyciagniety z 100 sortowan" << endl << "pierwsze " << proc[k] << "  elementow sa ustawione w kolejnosci:" << endl << endl;
		for (int j = 0;j < 100; j++)
		{
			if(j%5==0)
			cout << j <<"  ";
			if (proc[k] == - 1)
			{
				for (int i = 0;i < n[l];i++)
					sort1[i] = n[l] - i;
			}
			else
			{
				for (int i = 0;i < n[l] * proc[k];i++)
					sort1[i] = i;

				for (int i = n[l] * proc[k];i < n[l];i++)
					sort1[i] = (rand() % 100000000 + 1000000);
			}


			for (int i = 0;i < n[l];i++)
				sort2[i + 1] = sort1[i];

			for (int i = 0;i < n[l];i++)
				sort3.add_back(sort1[i]);


			start = clock();
			szybkie(sort1, 0, n[l] - 1);
			stop = clock();
			szyb[j] = (double)(stop - start) / CLOCKS_PER_SEC;

			start = clock();
			introspektywne(sort2, 1, n[l], (int)2 * log2(n[l]));
			stop = clock();
			intro[j] = (double)(stop - start) / CLOCKS_PER_SEC;

			start = clock();
			scal(sort3);
			stop = clock();
			scalanie[j] = (double)(stop - start) / CLOCKS_PER_SEC;
			sort3.delete_all();
		}

		srednia = 0;
		for (int i = 0;i < 100;i++)
			srednia = srednia + szyb[i];
		cout << "sortowanie szybkie srednia z 100 pomiarow: " << srednia / 100 << endl;

		srednia = 0;
		for (int i = 0;i < 100;i++)
			srednia = srednia + intro[i];
		cout << "sortowanie introspektywe  srednia z 100 pomiarow: " << srednia / 100 << endl;

		srednia = 0;
		for (int i = 0;i < 100;i++)
			srednia = srednia + scalanie[i];
		cout << "sortowanie przez scalanie srednia z 100 pomiarow: " << srednia / 100 << endl;
	}
			delete[] sort1;
			delete[] sort2;
		}


	}
	catch (string a)
	{
		cout << "wyjatek: " << a<<endl;
	}
	system("PAUSE");
}

